clean :
	rm -rf public
	rm -rf reveal.js
	git submodule add --force https://github.com/hakimel/reveal.js reveal.js

build:
	./build-themes.sh

run:
	./run-server.sh

ci:
	rm -rf public || true
	mkdir public
	cp -r custom/themes/fonts reveal.js/dist/theme
	cp -r custom/themes/images reveal.js/dist/theme
	cp custom/themes/*.scss reveal.js/css/theme/source
	cp -r images public
	sed 's%reveal.js/%%g' presentation.html > reveal.js/index.html
	cd reveal.js && \
	npm install && \
	npm run build -- css-themes && \
	sed -i "s/'eslint', 'qunit'/'eslint'/g" gulpfile.js && \
	gulp package && \
	unzip reveal-js-presentation.zip -d ../public