# Demos Berlin e.V. presentation

You can find the rendered presentation [here](https://demos-berlin-ev.gitlab.io/presentations/37c3).

# Description

[reveal.js](https://revealjs.com/) is a presentation framework, that utilized modern browser technologies to create a presentation as homepage.
This combines multiple things:
* reveal.js
* customized theme to apply the Demos corporate design
* multi column reveal pages
* GitLab CI pipeline to automatically build and deploy the presentation to [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/)

## Requirements

### Local

* [npm](https://www.npmjs.com/) is needed to build the Demos template
* a modern browser

If you have [Docker](https://www.docker.com/) installed, you do not need to
install [npm](https://www.npmjs.com/) and use a Docker image for building.

### GitLab CI

The GitLab CI will build the presentation an makes it avaible via [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/).

* [GitLab Runner](https://docs.gitlab.com/runner/) with [docker executor](https://docs.gitlab.com/runner/executors/docker.html)
    * for other executors you might need to adopt the [.gitlab-ci.yml](.gitlab-ci.yml)
* [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/) enabled for you project `(Project) Settings > General > Visibility, project features, permissions > Pages`

## Getting Started

1. fork the repository
1. clone your fork
1. build the template by running `./build-themes.sh`
1. put your images to [images folder](images/)
1. adopt [presentation.html](presentation.html) to your needs
    * every `<section> ... </section>` is a slide
    * you can write [markdown](https://www.markdownguide.org/) or [HTML](https://www.w3schools.com/html/)
    * select light or dark theme in the [presentation, line 11](presentation.html#L11)
1. view your presentation by opening the `presentation.html` in a browser
1. push to GitLab
1. you will find the link to your presentation here: `(Project) Settings > Pages`

### Live server

It is possible to run a local server, which refreshes the page imadatily on
changes in the presentation. Since [reveal.js](https://revealjs.com/) is a git
[submodule](https://www.git-scm.com/book/en/v2/Git-Tools-Submodules) the
presentation is modified and copied into the `reveal.js` directory. So when
running the live server you have to edit the `reveal.js/index.html`.

To run the live server use `./run-server.sh`. This will generate a new
`reveal.js/index.html` from `presentation.html`.

When editing the `reveal.js/index.html` the `presentation.html` must be
regenerated when using GitLab CI. To regenerate the `presentation.html` you can
run:

```shell
sed 's%dist/%reveal.js/dist/%g;s%plugin/%reveal.js/plugin/%g' reveal.js/index.html > presentation.html
```
